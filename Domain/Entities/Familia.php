<?php

namespace Domain\Entities;

use Domain\Entities\Interfaces\FamiliaInterface;
use Domain\Entities\Interfaces\PessoaInterface;

class Familia extends BaseEntity implements FamiliaInterface
{
    /**
     * @var array|Pessoa[]
     */
    private $pessoas;

    const PONTUACAO_POR_RENDA_ABAIXO_DE_900 = 5;

    const PONTUACAO_POR_RENDA_ENTRE_901_E_1500 = 3;

    const PONTUACAO_POR_RENDA_ENTRE_1501_E_2000 = 1;

    const PONTUACAO_POR_DEPENDENTES_COM_3_OU_MAIS = 3;

    const PONTUACAO_POR_DEPENDENTES_ENTRE_1_E_2   = 2;

    const INDICE_DO_PRETENDENTE = 'pretendente';

    const INDICE_DOS_DEPENDENTES = 'dependentes';

    const INDICE_DO_CONJUGE = 'conjuge';

    public function __construct(array $pessoas = [])
    {
        foreach ($pessoas as $pessoa) {
            $this->adicionarPessoa($pessoa);
        }
    }

    public function adicionarPessoa(PessoaInterface $pessoa): void
    {
        if ($pessoa->ePretendente()) {
            $this->pessoas[self::INDICE_DO_PRETENDENTE] = $pessoa;

            return;
        }

        if ($pessoa->eDependente()) {
            $this->adicionarDependente($pessoa);

            return;
        }

        $this->pessoas[self::INDICE_DO_CONJUGE] = $pessoa;
    }

    private function adicionarDependente(PessoaInterface $pessoa): void
    {
        if (isset($this->pessoas[self::INDICE_DOS_DEPENDENTES])) {
            $this->pessoas[self::INDICE_DOS_DEPENDENTES][] = $pessoa;
            return;
        }

        $this->pessoas[self::INDICE_DOS_DEPENDENTES]   = [];
        $this->pessoas[self::INDICE_DOS_DEPENDENTES][] = $pessoa;
    }

    public function obterPessoas() : array
    {
        return $this->pessoas;
    }

    private function obterRendaTotal(): float
    {
        $conjuge     = $this->pessoas[self::INDICE_DO_CONJUGE] ?? null;
        $pretendente = $this->pessoas[self::INDICE_DO_PRETENDENTE];

        if(!$conjuge) {
            return $pretendente->obterRendaTotal();
        }

        return $conjuge->obterRendaTotal() + $pretendente->obterRendaTotal();
    }

    public function obterPontuacaoDeAcordoComARenda(): int
    {
        $renda = $this->obterRendaTotal();

        if($renda >= 2001) {
            return 0;
        }

        if($renda <= 900) {
            return self::PONTUACAO_POR_RENDA_ABAIXO_DE_900;
        }

        if($renda <= 1501) {
            return self::PONTUACAO_POR_RENDA_ENTRE_901_E_1500;
        }

        return self::PONTUACAO_POR_RENDA_ENTRE_1501_E_2000;
    }

    private function obterAQuantidadeDeDependentes(): int
    {
        return count($this->pessoas[self::INDICE_DOS_DEPENDENTES] ?? []);
    }

    public function obterPontuacaoDeAcordoComAQuantidadeDeDependentes(): int
    {
        $quantidadeDeDependentes = $this->obterAQuantidadeDeDependentes();

        if($quantidadeDeDependentes === 0) {
            return 0;
        }

        if($quantidadeDeDependentes <= 2) {
            return self::PONTUACAO_POR_DEPENDENTES_ENTRE_1_E_2;
        }

        return self::PONTUACAO_POR_DEPENDENTES_COM_3_OU_MAIS;
    }

    public function obterPretendente(): PessoaInterface
    {
        return $this->pessoas[self::INDICE_DO_PRETENDENTE];
    }

    public function obterDependentes(): array
    {
        return $this->pessoas[self::INDICE_DOS_DEPENDENTES];
    }

    public function obterConjuge(): array
    {
        return $this->pessoas[self::INDICE_DO_CONJUGE];
    }

    public function obterPontuacaoTotal(): int
    {
        $pontuacaoPorRenda              = $this->obterPontuacaoDeAcordoComARenda();
        $pontuacaoPorDependentes        = $this->obterPontuacaoDeAcordoComAQuantidadeDeDependentes();
        $pontuacaoPorIdadeDoPretendente = $this->obterPretendente()->obterPontuacaoPorIdade();

        return $pontuacaoPorDependentes + $pontuacaoPorIdadeDoPretendente + $pontuacaoPorRenda;
    }
}