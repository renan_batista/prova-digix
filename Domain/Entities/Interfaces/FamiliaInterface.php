<?php

namespace Domain\Entities\Interfaces;

interface FamiliaInterface
{
    public function adicionarPessoa(PessoaInterface $pessoa) : void;

    public function obterPontuacaoDeAcordoComAQuantidadeDeDependentes(): int;
}