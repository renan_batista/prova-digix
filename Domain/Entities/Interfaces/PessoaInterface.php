<?php


namespace Domain\Entities\Interfaces;


use Carbon\Carbon;

interface PessoaInterface
{
    public function adicionarRenda(RendaInterface $renda): void;

    public function obterRendaTotal(): float;

    public function obterPontuacaoPorIdade(): int;

    public function setPretendente(bool $pretendente): void;

    public function ePretendente(): bool;

    public function getNome(): string;

    public function setNome(string $nome): void;

    public function getCpf(): string;

    public function setCpf(string $cpf): void;

    public function getDataDeNascimento(): Carbon;

    public function setDataDeNascimento(Carbon $dataDeNascimento): void;

    public function getRendas(): array;

    public function setRendas(array $rendas): void;

    public function eDependente(): bool;
}