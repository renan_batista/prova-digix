<?php
namespace Domain\Entities\Interfaces;

interface RendaInterface
{
    public function setNome(string $nome): void;

    public function setValor(float $valor): void;

    public function getNome(): ?string;

    public function getValor(): ?float;
}