<?php

namespace Domain\Entities;

use Carbon\Carbon;
use Domain\Entities\Interfaces\PessoaInterface;
use Domain\Entities\Interfaces\RendaInterface;

class Pessoa implements PessoaInterface
{
    /**
     * @var string
     */
    private $nome;

    /***
     * @var string
     */
    private $cpf;

    /**
     * @var Carbon
     */
    private $dataDeNascimento;

    /**
     * @var bool
     */
    private $pretendente;

    /**
     * @var array|Renda[]
     */
    private $rendas;

    /**
     * @var bool
     */
    private $dependente;

    /**
     * @var PessoaInterface|null
     */
    private $conjuge;

    const PONTUACAO_POR_IDADE_ACIMA_DE_45_ANOS = 3;

    const PONTUACAO_POR_IDADE_ABAIXO_DE_30_ANOS = 1;

    const PONTUACAO_POR_IDADE_ENTRE_31_E_44_ANOS = 2;

    public function __construct(
        string $nome = null,
        string $cpf = null,
        Carbon $dataDeNascimento = null,
        array $rendas = [],
        bool $pretendente = false,
        bool $dependente  = false
    ) {
        $this->nome             = $nome;
        $this->cpf              = $cpf;
        $this->dataDeNascimento = $dataDeNascimento;
        $this->rendas           = $rendas;
        $this->pretendente      = $pretendente;
        $this->dependente       = $dependente;
    }

    public function adicionarRenda(RendaInterface $renda): void
    {
        $this->rendas[] = $renda;
    }

    public function obterRendaTotal(): float
    {
        $total = 0.0;

        foreach ($this->rendas as $renda) {
            $total += $renda->getValor();
        }

        return $total;
    }

    public function obterPontuacaoPorIdade(): int
    {
        if(!$this->ePretendente()) {
            return 0;
        }

        $dataAtual = Carbon::now();

        $anosDeIdade = $this->dataDeNascimento->diffInYears($dataAtual);

        if ($anosDeIdade >= 45) {
            return self::PONTUACAO_POR_IDADE_ACIMA_DE_45_ANOS;
        }

        if ($anosDeIdade <= 30) {
            return self::PONTUACAO_POR_IDADE_ABAIXO_DE_30_ANOS;
        }

        return self::PONTUACAO_POR_IDADE_ENTRE_31_E_44_ANOS;
    }

    public function eDependente(): bool
    {
        $dataAtual = Carbon::now();
        $idadeEmAnos = $this->dataDeNascimento->diffInYears($dataAtual);

        return $idadeEmAnos < 18;
    }

    public function setPretendente(bool $pretendente): void
    {
        $this->pretendente = $pretendente;
    }

    public function ePretendente(): bool
    {
        return $this->pretendente;
    }

    public function getNome(): string
    {
        return $this->nome;
    }

    public function setNome(string $nome): void
    {
        $this->nome = $nome;
    }

    public function getCpf(): string
    {
        return $this->cpf;
    }

    public function setCpf(string $cpf): void
    {
        $this->cpf = $cpf;
    }

    public function getDataDeNascimento(): Carbon
    {
        return $this->dataDeNascimento;
    }

    public function setDataDeNascimento(Carbon $dataDeNascimento): void
    {
        $this->dataDeNascimento = $dataDeNascimento;
    }

    public function getRendas(): array
    {
        return $this->rendas;
    }

    public function setRendas(array $rendas): void
    {
        $this->rendas = $rendas;
    }
}