<?php
namespace Domain\Entities;

use Domain\Entities\Interfaces\RendaInterface;

class Renda extends BaseEntity implements RendaInterface
{
    /**
     * @var string
     */
    private $nome;

    /**
     * @var float
     */
    private $valor;

    public function __construct(string $nome = '', float $valor = null)
    {
        $this->nome  = $nome;
        $this->valor = $valor;
    }

    /**
     * @param string $nome
     */
    public function setNome(string $nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @param float $valor
     */
    public function setValor(float $valor): void
    {
        $this->valor = $valor;
    }

    /**
     * @return string
     */
    public function getNome(): ?string
    {
        return $this->nome;
    }

    /**
     * @return float
     */
    public function getValor(): ?float
    {
        return $this->valor;
    }
}