<?php


namespace Tests\Unit\Domain\Builders\Entities;


use Faker\Generator;

abstract class BaseBuilder implements Builder
{
    /**
     * @var Generator
     */
    protected $faker;

    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
    }
}