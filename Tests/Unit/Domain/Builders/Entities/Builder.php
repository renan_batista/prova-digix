<?php

namespace Tests\Unit\Domain\Builders\Entities;

use Mockery\MockInterface;

interface Builder
{
    public function obter();

    public function criar();

    public function mock(): MockInterface;
}