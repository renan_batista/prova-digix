<?php

namespace Tests\Unit\Domain\Builders\Entities;

use Domain\Entities\Familia;
use Domain\Entities\Interfaces\FamiliaInterface;
use Mockery\MockInterface;

class FamiliaBuilder extends BaseBuilder
{
    /**
     * @var Familia
     */
    private $familia;

    public function obter(): Familia
    {
        return $this->familia;
    }

    public function criar(): FamiliaBuilder
    {
        $this->familia = new Familia();
        return $this;
    }

    public function mock(): MockInterface
    {
        return \Mockery::mock(FamiliaInterface::class);
    }

    public function mockParcial(): MockInterface
    {
        return \Mockery::mock(Familia::class)->makePartial();
    }
}