<?php

namespace Tests\Unit\Domain\Builders\Entities;

use Carbon\Carbon;
use Domain\Entities\Pessoa;
use Domain\Entities\Renda;
use Domain\Entities\Interfaces\PessoaInterface;
use Mockery\MockInterface;

class PessoaBuilder extends BaseBuilder
{
    /**
     * @var Pessoa
     */
    private $pessoa;

    public function criar(): PessoaBuilder
    {
        $this->pessoa = new Pessoa();
        return $this;
    }

    public function obter(): Pessoa
    {
        return $this->pessoa;
    }

    public function comNome(string $nome = null): PessoaBuilder
    {
        $this->pessoa->setNome($nome ?? $this->faker->name);
        return $this;
    }

    public function comCpf(string $cpf = null): PessoaBuilder
    {
        $this->pessoa->setCpf($cpf ?? $this->faker->cpf());
        return $this;
    }

    public function comADataDeNascimento(Carbon $dataDeNascimento = null): PessoaBuilder
    {
        $this->pessoa->setDataDeNascimento($dataDeNascimento ?? Carbon::createFromFormat('Y-m-d', $this->faker->date()));
        return $this;
    }

    public function comRenda(Renda $renda = null): PessoaBuilder
    {
        $this->pessoa->adicionarRenda($renda);
        return $this;
    }

    public function sendoPretendente(): PessoaBuilder
    {
        $this->pessoa->setPretendente(true);
        return $this;
    }

    public function sendoDependente(): PessoaBuilder
    {
        $this->pessoa->setDependente(true);
        return $this;
    }

    public function mock(): MockInterface
    {
        return \Mockery::mock(PessoaInterface::class);
    }
}