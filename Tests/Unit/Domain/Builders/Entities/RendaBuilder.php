<?php

namespace Tests\Unit\Domain\Builders\Entities;

use Domain\Entities\Renda;
use Domain\Entities\Interfaces\RendaInterface;
use Mockery\MockInterface;

class RendaBuilder extends BaseBuilder
{
    /**
     * @var Renda
     */
    private $renda;

    public function obter(): Renda
    {
        return $this->renda;
    }

    public function criar(): RendaBuilder
    {
        $this->renda = new Renda();
        return $this;
    }

    public function comNome(string $nome = null)
    {
        $this->renda->setNome($nome ?? $this->faker->name);
        return $this;
    }

    public function comValor(float $valor = null)
    {
        $this->renda->setValor($valor ?? $this->faker->randomFloat(2, 0, 9999));
        return $this;
    }

    public function mock(): MockInterface
    {
        return \Mockery::mock(RendaInterface::class);
    }
}