<?php


namespace Tests\Unit\Domain;


use Tests\Unit\Domain\Builders\Entities\FamiliaBuilder;
use Tests\Unit\Domain\Builders\Entities\PessoaBuilder;
use Tests\Unit\Domain\Builders\Entities\RendaBuilder;
use Tests\Unit\UnitTestCase;

abstract class DomainBaseTest extends UnitTestCase
{
    /**
     * @var PessoaBuilder
     */
    protected $pessoaBuilder;

    /**
     * @var FamiliaBuilder
     */
    protected $familiaBuilder;

    /**
     * @var RendaBuilder
     */
    protected $rendaBuilder;

    public function setUp(): void
    {
        parent::setUp();

        $this->pessoaBuilder  = new PessoaBuilder($this->faker);
        $this->familiaBuilder = new FamiliaBuilder($this->faker);
        $this->rendaBuilder   = new RendaBuilder($this->faker);
    }
}