<?php

namespace Tests\Unit\Domain;

use Domain\Entities\Familia;
use Domain\Entities\Pessoa;
use Mockery\MockInterface;

class FamiliaTest extends DomainBaseTest
{

    /**
     * @return array
     */
    public function pontuacoesEsperadasDeAcordoComARenda(): array
    {
        $this->setUp();

        return [
            [
                [
                    'pontuacao_esperada' => Familia::PONTUACAO_POR_RENDA_ABAIXO_DE_900,
                    'renda_minima'       => 0,
                    'renda_maxima'       => 900
                ]
            ],
            [
                [
                    'pontuacao_esperada' => Familia::PONTUACAO_POR_RENDA_ENTRE_901_E_1500,
                    'renda_minima'       => 901,
                    'renda_maxima'       => 1500
                ]
            ],
            [
                [
                    'pontuacao_esperada' => Familia::PONTUACAO_POR_RENDA_ENTRE_1501_E_2000,
                    'renda_minima'       => 1501,
                    'renda_maxima'       => 2000
                ]
            ],
            [
                [
                    'pontuacao_esperada' => 0,
                    'renda_minima'       => 2001,
                    'renda_maxima'       => null
                ]
            ]
        ];
    }

    private function atribuirCenarioParaPessoaNaoPretendente(MockInterface $pessoa)
    {
        $pessoa->shouldReceive('ePretendente')
            ->andReturn(false);
    }

    private function atribuirCenarioParaPessoaNaoDependente(MockInterface $pessoa)
    {
        $pessoa->shouldReceive('eDependente')
            ->andReturn(false);
    }

    /**
     * @test
     * @dataProvider pontuacoesEsperadasDeAcordoComARenda
     */
    public function devePontuarDeAcordoComARendaDoConjugeEDoPretentente(array $resultadoEsperado)
    {
        $rendaTotalDaFamilia = $this->faker->randomFloat(2, $resultadoEsperado['renda_minima'],
                                                         $resultadoEsperado['renda_maxima'] ?? 99999);

        $rendaPorIndividuo = $rendaTotalDaFamilia / 2;

        $familia = $this->familiaBuilder->criar()->obter();

        $pretentente = $this->pessoaBuilder->mock();
        $conjuge     = $this->pessoaBuilder->mock();

        $pretentente->shouldReceive('obterRendaTotal')->andReturn($rendaPorIndividuo);
        $conjuge->shouldReceive('obterRendaTotal')->andReturn($rendaPorIndividuo);
        $this->atribuirCenarioParaPessoaNaoDependente($pretentente);
        $pretentente->shouldReceive('ePretendente')->andReturn(true);

        $this->atribuirCenarioParaPessoaNaoDependente($conjuge);
        $this->atribuirCenarioParaPessoaNaoPretendente($conjuge);

        $familia->adicionarPessoa($pretentente);
        $familia->adicionarPessoa($conjuge);

        $this->assertEquals($resultadoEsperado['pontuacao_esperada'], $familia->obterPontuacaoDeAcordoComARenda());
    }

    /**
     * @test
     * @dataProvider pontuacoesEsperadasDeAcordoComARenda
     */
    public function devePontuarDeAcordoComARendaDoPretentente(array $resultadoEsperado)
    {
        $rendaTotalDaFamilia = $this->faker->randomFloat(2, $resultadoEsperado['renda_minima'],
                                                         $resultadoEsperado['renda_maxima'] ?? 99999);

        $familia = $this->familiaBuilder->criar()->obter();

        $pretentente = $this->pessoaBuilder->mock();

        $pretentente->shouldReceive('obterRendaTotal')->andReturn($rendaTotalDaFamilia);
        $this->atribuirCenarioParaPessoaNaoDependente($pretentente);
        $pretentente->shouldReceive('ePretendente')->andReturn(true);

        $familia->adicionarPessoa($pretentente);

        $this->assertEquals($resultadoEsperado['pontuacao_esperada'], $familia->obterPontuacaoDeAcordoComARenda());
    }

    /**
     * @return array
     */
    public function pontuacoesEsperadasDeAcordoComAQuantidadeDeDependentes(): array
    {
        return [
            [
                [
                    'pontuacao_esperada' => Familia::PONTUACAO_POR_DEPENDENTES_COM_3_OU_MAIS,
                    'quantidade_minima'  => 3,
                    'quantidade_maxima'  => 99
                ]
            ],
            [
                [
                    'pontuacao_esperada' => Familia::PONTUACAO_POR_DEPENDENTES_ENTRE_1_E_2,
                    'quantidade_minima'  => 1,
                    'quantidade_maxima'  => 2
                ]
            ],
            [
                [
                    'pontuacao_esperada' => 0,
                    'quantidade_minima'  => 0,
                    'quantidade_maxima'  => 0
                ]
            ]
        ];
    }

    /**
     * @test
     * @dataProvider pontuacoesEsperadasDeAcordoComAQuantidadeDeDependentes
     */
    public function deveObterAPontuacaoDeAcordoComAQuantidadeDeDependentes(array $resultadoEsperado)
    {
        $quantidadeDeDependentes = $this->faker->numberBetween(
            $resultadoEsperado['quantidade_minima'],
            $resultadoEsperado['quantidade_maxima']
        );

        $familia = $this->familiaBuilder->criar()->obter();

        for ($i = 0; $i < $quantidadeDeDependentes; $i++) {
            $pessoa = $this->pessoaBuilder->mock();
            $pessoa->shouldReceive('eDependente')->andReturn(true);
            $this->atribuirCenarioParaPessoaNaoPretendente($pessoa);

            $familia->adicionarPessoa($pessoa);
        }

        $this->assertEquals($resultadoEsperado['pontuacao_esperada'],
                            $familia->obterPontuacaoDeAcordoComAQuantidadeDeDependentes());
    }


    /**
     * @return array
     */
    public function pontuacoesTotaisEsperadas(): array
    {
        return [
            [
                [
                    'pontuacao_esperada' => Familia::PONTUACAO_POR_DEPENDENTES_COM_3_OU_MAIS,
                    'quantidade_minima'  => 3,
                    'quantidade_maxima'  => null
                ]
            ],
            [
                [
                    'pontuacao_esperada' => Familia::PONTUACAO_POR_DEPENDENTES_ENTRE_1_E_2,
                    'quantidade_minima'  => 1,
                    'quantidade_maxima'  => 2
                ]
            ],
            [
                [
                    'pontuacao_esperada' => 0,
                    'quantidade_minima'  => 0,
                    'quantidade_maxima'  => 0
                ]
            ]
        ];
    }
    /**
     * @test
     */
    public function deveObterAPontuacaoTotal()
    {
        $pontuacaoPorIdadeDoPretendente = $this->faker->randomElement(
            [
                Pessoa::PONTUACAO_POR_IDADE_ABAIXO_DE_30_ANOS,
                Pessoa::PONTUACAO_POR_IDADE_ACIMA_DE_45_ANOS,
                Pessoa::PONTUACAO_POR_IDADE_ENTRE_31_E_44_ANOS,
                0
            ]
        );

        $pontuacaoPorRenda = $this->faker->randomElement(
            [
                Familia::PONTUACAO_POR_RENDA_ABAIXO_DE_900,
                Familia::PONTUACAO_POR_RENDA_ENTRE_901_E_1500,
                Familia::PONTUACAO_POR_RENDA_ENTRE_1501_E_2000,
                0
            ]
        );

        $pontuacaoPorDependentes = $this->faker->randomElement(
            [
                Familia::PONTUACAO_POR_DEPENDENTES_COM_3_OU_MAIS,
                Familia::PONTUACAO_POR_DEPENDENTES_ENTRE_1_E_2,
                0
            ]
        );

        $resultadoEsperado = $pontuacaoPorIdadeDoPretendente + $pontuacaoPorRenda + $pontuacaoPorDependentes;

        $familia = $this->familiaBuilder->mockParcial();
        $pessoa  = $this->pessoaBuilder->mock();

        $pessoa
            ->shouldReceive('ePretendente')
            ->andReturn(true)
            ->once();

        $familia->adicionarPessoa($pessoa);

        $pessoa->shouldReceive('obterPontuacaoPorIdade')->andReturn($pontuacaoPorIdadeDoPretendente)->once();
        $familia->shouldReceive('obterPontuacaoDeAcordoComAQuantidadeDeDependentes')->andReturn($pontuacaoPorDependentes)->once();
        $familia->shouldReceive('obterPontuacaoDeAcordoComARenda')->andReturn($pontuacaoPorRenda)->once();

        $resultado = $familia->obterPontuacaoTotal();

        $this->assertEquals($resultadoEsperado, $resultado);
    }

    /**
     * @test
     */
    public function deveAdicionarUmPretendenteNoIndiceCorrespondente()
    {
        $familia = $this->familiaBuilder->criar()->obter();
        $pessoa  = $this->pessoaBuilder->mock();

        $pessoa
            ->shouldReceive('ePretendente')
            ->andReturn(true)
            ->once();

        $familia->adicionarPessoa($pessoa);

        $resultadoEsperado = [
            Familia::INDICE_DO_PRETENDENTE => $pessoa
        ];

        $this->assertEquals($resultadoEsperado, $familia->obterPessoas());
    }

    /**
     * @test
     */
    public function deveAdicionarDependentesNoIndiceCorrespondente()
    {
        $familia                 = $this->familiaBuilder->criar()->obter();
        $quantidadeDeDependentes = $this->faker->numberBetween(1, 5);
        $i = 0;
        $dependentes = [];

        while ($i < $quantidadeDeDependentes) {
            $pessoa                  = $this->pessoaBuilder->mock();
            $this->atribuirCenarioParaPessoaNaoPretendente($pessoa);

            $pessoa
                ->shouldReceive('eDependente')
                ->andReturn(true)
                ->once();

            $familia->adicionarPessoa($pessoa);

            $dependentes[] = $pessoa;

            $i++;
        }

        $resultadoEsperado = [
            Familia::INDICE_DOS_DEPENDENTES => $dependentes
        ];

        $this->assertEquals($resultadoEsperado, $familia->obterPessoas());
    }

    /**
     * @test
     */
    public function deveAdicionarOConjugeNoIndiceCorrespondente()
    {
        $familia = $this->familiaBuilder->criar()->obter();
        $pessoa  = $this->pessoaBuilder->mock();

        $this->atribuirCenarioParaPessoaNaoPretendente($pessoa);
        $this->atribuirCenarioParaPessoaNaoDependente($pessoa);

        $familia->adicionarPessoa($pessoa);

        $resultadoEsperado = [
            Familia::INDICE_DO_CONJUGE => $pessoa
        ];

        $this->assertEquals($resultadoEsperado, $familia->obterPessoas());
    }
}