<?php

namespace Tests\Unit\Domain;

use Carbon\Carbon;
use Domain\Entities\Pessoa;

class PessoaTest extends DomainBaseTest
{
    /**
     * @test
     */
    public function deveObterARendaTotal()
    {
        $quantidadeDeFontesDeRendaDaPessoa = $this->faker->numberBetween(1, 10);

        $contador = 0;
        $totalEsperado = 0;

        $pessoa = $this->pessoaBuilder
            ->criar()
            ->comNome()
            ->comCpf()
            ->comADataDeNascimento()
            ->obter();

        while ($contador < $quantidadeDeFontesDeRendaDaPessoa) {
            $valorDaRenda = $this->faker->randomFloat(2, 0, 99999);

            $renda = $this->rendaBuilder->mock();
            $renda->shouldReceive('getValor')
                ->andReturn($valorDaRenda)
                ->once();

            $pessoa->adicionarRenda($renda);
            $totalEsperado += $valorDaRenda;
            $contador++;
        }

        $this->assertEquals($totalEsperado, $pessoa->obterRendaTotal());
    }

    /**
     * @test
     */
    public function deveObterUmValorZeradoSeAPessoaNaoPossuirRenda()
    {
        $pessoa = $this->pessoaBuilder
            ->criar()
            ->comNome()
            ->comCpf()
            ->comADataDeNascimento()
            ->obter();

        $this->assertEquals(0, $pessoa->obterRendaTotal());
    }

    /**
     * @return array
     */
    public function pontuacoesEsperadasDeAcordoComAIdadeDoPretendente(): array
    {
        $this->setUp();

        return [
            [
                [
                    'pontuacao_esperada' => Pessoa::PONTUACAO_POR_IDADE_ABAIXO_DE_30_ANOS,
                    'idade_minima'       => 0,
                    'idade_maxima'       => 30
                ]
            ],
            [
                [
                    'pontuacao_esperada' => Pessoa::PONTUACAO_POR_IDADE_ENTRE_31_E_44_ANOS,
                    'idade_minima'       => 31,
                    'idade_maxima'       => 44
                ]
            ],
            [
                [
                    'pontuacao_esperada' => Pessoa::PONTUACAO_POR_IDADE_ACIMA_DE_45_ANOS,
                    'idade_minima'       => 45,
                    'idade_maxima'       => 150
                ]
            ]
        ];
    }

    /**
     * @test
     * @dataProvider pontuacoesEsperadasDeAcordoComAIdadeDoPretendente
     */
    public function deveObterUmaPontuacaoDeAcordoComAIdadeDoPretendente(array $resultadoEsperado)
    {
        $dataDeNascimento = Carbon::now();
        $idade            = $this->faker->numberBetween(
            $resultadoEsperado['idade_minima'],
            $resultadoEsperado['idade_maxima']
        );

        $dataDeNascimento->subYears($idade);

        $pessoa = $this->pessoaBuilder
            ->criar()
            ->sendoPretendente()
            ->comADataDeNascimento($dataDeNascimento)
            ->obter();

        $pontuacao = $pessoa->obterPontuacaoPorIdade();
        $this->assertEquals($resultadoEsperado['pontuacao_esperada'], $pontuacao);
    }

    /**
     * @test
     */
    public function deveObterUmaPontuacaoZeradaPorIdadeSeAPessoaNaoForPretendente()
    {
        $pontuacaoEsperada = 0;

        $pessoa = $this->pessoaBuilder
            ->criar()
            ->comADataDeNascimento()
            ->obter();

        $pontuacao = $pessoa->obterPontuacaoPorIdade();
        $this->assertEquals($pontuacaoEsperada, $pontuacao);
    }

    /**
     * @test
     */
    public function deveSerDependenteSeAPessoaForMenorDeIdade()
    {
        $dataDeNascimento = Carbon::now();
        $dataDeNascimento->subYears($this->faker->numberBetween(0, 17));

        $pessoa = $this->pessoaBuilder
            ->criar()
            ->comADataDeNascimento($dataDeNascimento)
            ->obter();

        $this->assertTrue($pessoa->eDependente());
    }

    /**
     * @test
     */
    public function naoDeveSerDependenteSeAPessoaForMaiorDeIdade()
    {
        $dataDeNascimento = Carbon::now();
        $dataDeNascimento->subYears($this->faker->numberBetween(18));

        $pessoa = $this->pessoaBuilder
            ->criar()
            ->comADataDeNascimento($dataDeNascimento)
            ->obter();

        $this->assertFalse($pessoa->eDependente());
    }
}