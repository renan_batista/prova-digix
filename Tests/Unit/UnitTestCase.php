<?php

namespace Tests\Unit;

use Faker\Factory;
use Faker\Generator;
use Faker\Provider\pt_BR\Person;
use PHPUnit\Framework\TestCase;

class UnitTestCase extends TestCase
{
    /**
     * @var Generator
     */
    protected $faker;

    public function setUp(): void
    {
        parent::setUp();
        $this->faker = Factory::create();
        $this->faker->addProvider(new Person($this->faker));
    }
}